package main

import (
	"bufio"
	"io"
	"log"
	"net"
	"os"
	"fmt"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:7567")

	if err != nil {
		log.Fatal(fmt.Sprintf("Err dialing server: %s\n", err.Error()))
	}

	console := make(chan string, 1)
	c := make(chan int, 1)

	go StartReadConsoleOperation(console)
	go StartSendMessagesOperation(conn, console)
	go StartReadMessagesOperation(conn)
	
	<-c

	conn.Close()
	log.Printf("Connection Closed")
}

func StartSendMessagesOperation(conn net.Conn, console chan string) {
	for {
		message := <-console
		_, err := conn.Write([]byte(message))

		if err != nil {
			log.Printf("Failed to deliver message: %v.\n", err)
		}

	}
}

func StartReadMessagesOperation(conn net.Conn) {
	for {
		io.Copy(os.Stdout, conn)
	}
}

func StartReadConsoleOperation(c chan string) {

	//create reader for console input
	console := bufio.NewReader(os.Stdin)

	for {
		line, err := console.ReadString('\n')

		if err != nil {

			log.Printf("Failed to read console input: %v.\n", err)

		} else {

			//send console input to network
			c <- line
		}
	}

}
