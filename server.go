package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

const COMMAND_RENAME string = "Name"
const COMMAND_LIST string = "List"
const COMMAND_BROADCAST string = "Everyone"
const COMMAND_VIEW_PROFILE = "About"
const COMMAND_LEAVE = "Leave"

const MESSAGE_BUZZ string = "Buzz"

var onlineContacts map[string]*Contact

type Profile struct {
	Name string
}

func (self *Profile) String() string {
	return fmt.Sprintf("%s's Profile:\n-Name:\t%s.\n", self.Name, self.Name)
}

type ProfileChange struct {
	RemoteAddr string
	OldProfile Profile
	NewProfile Profile
}

type Contact struct {
	Profile    Profile
	Connection net.Conn
	LastSeen   time.Time
}

func (self *Contact) StartChatOperation(server *Server) {
	for {
		reader := bufio.NewReader(self.Connection)
		all, _ := reader.ReadString('\n')

		if all != "" {
			all = strings.Replace(all, "\n", "", -1)

			tokens := strings.Split(all, ":")

			if len(tokens) != 2 {
				self.Connection.Write([]byte("Message could not be delivered. Invalid structure.\n"))
				continue
			}

			var command string = tokens[0]
			var data string = tokens[1]

			if command == COMMAND_RENAME {

				oldProfile := self.Profile
				newProfile := self.Profile
				newProfile.Name = data
				profileChange := ProfileChange{OldProfile: oldProfile, NewProfile: newProfile, RemoteAddr: self.Connection.RemoteAddr().String()}

				server.RenameContactChannel <- profileChange

			} else if command ==COMMAND_LIST {

				self.Connection.Write([]byte(server.AvailableContactsString()))

			} else if recepient := server.LookUp(command); recepient != nil {

				message := &Message{From: self, To: recepient, Body: data}
				server.MessageDeliveryChannel <- message

			} else if command == COMMAND_BROADCAST {

				message := &Message{From: self, To: nil, Body: data}
				server.Broadcast(message.String())

			} else if command == COMMAND_VIEW_PROFILE {

				if person := server.LookUp(data); person != nil {
					self.Connection.Write([]byte(person.Profile.String()))
				} else {
					self.Connection.Write([]byte(fmt.Sprintf("Can't find %s's profile.\n", data)))
				}

			}else if command == COMMAND_LEAVE {

				server.RemoveContactChannel <- self

			}
		}

	}
}

type Message struct {
	From *Contact
	To   *Contact
	Body string
}

func (self *Message) String() string {
	return fmt.Sprintf("> %s: %s.\n", self.From.Profile.Name, self.Body)
}

type Server struct {
	net.Listener
	RemoveContactChannel   chan *Contact
	RenameContactChannel   chan ProfileChange
	MessageDeliveryChannel chan *Message
}

func (self *Server) LookUp(profileName string) *Contact {
	for _, contact := range onlineContacts {
		if contact.Profile.Name == profileName {
			return contact
		}
	}

	return nil
}

func (self *Server) StartAcceptConnectionsOperation() {
	for {

		conn, err := self.Accept()

		if err != nil {
			log.Printf("Error Accepting Connection: %v.\n", err)
		}

		contact := &Contact{Connection: conn, Profile: Profile{Name: conn.RemoteAddr().String()}}

		onlineContacts[contact.Connection.RemoteAddr().String()] = contact

		self.Broadcast(fmt.Sprintf("%s has joined the chat.\n", contact.Profile.Name))
		log.Printf("Connection accepted from %s.\n", contact.Profile.Name)

		go contact.StartChatOperation(self)
	}
}

func (self *Server) StartRemoveContactOperation() {

	for {
		removee := <-self.RemoveContactChannel

		removee.Connection.Write([]byte("Bye!"))
		removee.Connection.Close()

		delete(onlineContacts, removee.Connection.RemoteAddr().String())

		self.Broadcast(fmt.Sprintf("%s has left the chat.\n", removee.Profile.Name))
	}
}

func (self *Server) StartRenameContactOperation() {

	for {
		profileChange := <-self.RenameContactChannel

		onlineContacts[profileChange.RemoteAddr].Profile = profileChange.NewProfile

		self.Broadcast(fmt.Sprintf("%s is now known as %s.\n", profileChange.OldProfile.Name, profileChange.NewProfile.Name))

	}
}

func (self *Server) StartMessageDeliveryOperation() {

	for {

		message := <-self.MessageDeliveryChannel
		recepient := onlineContacts[message.To.Connection.RemoteAddr().String()]

		if message.Body == MESSAGE_BUZZ {
			message.Body += "\a"
		}

		_, err := recepient.Connection.Write([]byte(message.String()))

		if err != nil {
			log.Printf("Failed to send message '%s' to %s.\n", message, recepient.Profile.Name)
		} else {
			log.Printf("Message '%s' transmitted to %s.\n", message, recepient.Profile.Name)
		}
	}
}

func (self *Server) Broadcast(message string) {

	for _, contact := range onlineContacts {

		_, err := contact.Connection.Write([]byte(message))

		if err != nil {
			log.Printf("Failed to deliver broadcast to %s.\n", contact.Profile.Name)
		} else {
			log.Printf("Broadcast delivered to %s.\n", contact.Profile.Name)
		}

	}
}

func (self *Server) AvailableContactsString() string {

	var availableContactsList bytes.Buffer

	for _, contact := range onlineContacts {
		availableContactsList.WriteString(fmt.Sprintf("%s : %s\n", contact.Profile.Name, contact.Connection.RemoteAddr().String()))
	}

	return availableContactsList.String()
}

func init() {
	onlineContacts = make(map[string]*Contact)
}

func main() {

	//create server
	listener, err := net.Listen("tcp", "127.0.0.1:7567")

	//if creating server fails, report error
	if err != nil {
		log.Printf("Error Establishing Server: %v.\n", err)
	}

	server := Server{
		Listener:               listener,
		RenameContactChannel:   make(chan ProfileChange),
		RemoveContactChannel:   make(chan *Contact),
		MessageDeliveryChannel: make(chan *Message)}

	fmt.Printf("Listening on %s.\n", listener.Addr().String())

	c := make(chan int)
	go server.StartRemoveContactOperation()
	go server.StartRenameContactOperation()
	go server.StartMessageDeliveryOperation()
	go server.StartAcceptConnectionsOperation()
	<- c

	fmt.Println("Server Closed.")
}
