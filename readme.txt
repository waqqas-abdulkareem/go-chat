A simple chat application using sockets, implemented in Go.

A contact can assign a nickname for himself by typing:
Name:<New NickName>
By default, a contact's username is his full IP Address and Port.

>Name:Marge
>127.0.0.1:49899 is now known as Marge.

A contact can view a list of contacts available by typing:
List:

>List:
>Waqqas : 127.0.0.1:49894
>127.0.0.1:49899 : 127.0.0.1:49899

Contacts can send messages to one another by entering the recepients name followed by the message.
>Marge:Hi!
>Marge:  What's Happening?.
>Marge: Nothing Much!

There are a few issues:
2. Code's a mess. Too many if statements. I want to refactor that before moving forward.
3. Not sure if using a Goroutine for each individual operation is a good idea. I'll try alternatives in tag 3.

Other features to implement\
1. Availability Status
2. Profile Change.